let SessionLoad = 1
if &cp | set nocp | endif
let s:cpo_save=&cpo
set cpo&vim
inoremap <silent> <expr> <BS> coc#_insert_key('request', 'iPGJzPg==0')
inoremap <silent> <expr> <C-.> coc#refresh()
inoremap <expr> <S-Tab> pumvisible() ? "\" : "\"
imap <C-G>S <Plug>ISurround
imap <C-G>s <Plug>Isurround
imap <C-S> <Plug>Isurround
inoremap <silent> <Plug>(fzf-maps-i) :call fzf#vim#maps('i', 0)
inoremap <expr> <Plug>(fzf-complete-buffer-line) fzf#vim#complete#buffer_line()
inoremap <expr> <Plug>(fzf-complete-line) fzf#vim#complete#line()
inoremap <expr> <Plug>(fzf-complete-file-ag) fzf#vim#complete#path('ag -l -g ""')
inoremap <expr> <Plug>(fzf-complete-file) fzf#vim#complete#path("find . -path '*/\.*' -prune -o -type f -print -o -type l -print | sed 's:^..::'")
inoremap <expr> <Plug>(fzf-complete-path) fzf#vim#complete#path("find . -path '*/\.*' -prune -o -print | sed '1d;s:^..::'")
inoremap <expr> <Plug>(fzf-complete-word) fzf#vim#complete#word()
inoremap <silent> <Plug>CocRefresh =coc#_complete()
inoremap <Right> <Nop>
inoremap <Left> <Nop>
inoremap <Down> <Nop>
inoremap <Up> <Nop>
inoremap <C-L> l
inoremap <C-K> k
inoremap <C-J> j
inoremap <C-H> h
inoremap <silent> <F2> =strftime("%d-%m-%Y")
map! <D-v> *
nnoremap <silent>  :let @/=""
nnoremap <silent>  :Rgw
xnoremap  h
tnoremap  h
nnoremap  h
xnoremap <NL> j
tnoremap <NL> j
nnoremap <NL> j
xnoremap  k
tnoremap  k
nnoremap  k
xnoremap  l
tnoremap  l
nnoremap  l
nnoremap <silent>  :Files
nnoremap t "=strftime("%c")P
nnoremap <silent> b :Bck
nnoremap r :GrepWiki 
nnoremap p :Files ~/git/md_wiki
nmap <silent> x <Plug>Commentary
xmap <silent>  <Plug>Commentary
tnoremap  
vmap  e <Plug>(ScalpelVisual)
nmap  e <Plug>(Scalpel)
nmap  w m <Plug>VimwikiMakeTomorrowDiaryNote
nmap  w y <Plug>VimwikiMakeYesterdayDiaryNote
nmap  w t <Plug>VimwikiTabMakeDiaryNote
nmap  w w <Plug>VimwikiMakeDiaryNote
nmap  w i <Plug>VimwikiDiaryGenerateLinks
nmap  wi <Plug>VimwikiDiaryIndex
nmap  ws <Plug>VimwikiUISelect
nmap  ww <Plug>VimwikiIndex
nmap  <F8> <Plug>VimspectorRunToCursor
nmap  <F9> <Plug>VimspectorToggleConditionalBreakpoint
nnoremap  r :exec '!python' shellescape(@%, 1)
nnoremap <silent>   t :split term://zsh:resize 20
nnoremap   ] :vsp :exec("tag ".expand("<cword>"))
nnoremap <silent>  bc :BCommits
nnoremap <silent>  cc :Commits
nnoremap <silent>  wt :Tags
nnoremap <silent>  d :GFiles
nnoremap <silent>  t :GGrep
nnoremap <silent>  b :Buffers
nnoremap <silent>  gh :VimwikiAll2HTML
nnoremap <silent>  l :VimwikiBacklinks
nnoremap  ss :VimwikiSearchTags 
nnoremap  re :GetRecipe
nnoremap <silent>  m :TodoToggle
nnoremap <silent>  tt :TodoBrowser
nnoremap  fD :FlutterVisualDebug
nnoremap  fr :FlutterHotRestart
nnoremap  fq :FlutterQuit
nnoremap  fa :FlutterRun
nnoremap <silent>  <F2> :call vimspector#AddWatch( input( 'Watch expression: ' ))
nnoremap <silent>  <F1> :call vimspector#Evaluate( input( 'Evaluate expression: ' ))
nnoremap <silent>  de :call vimspector#Reset()
nnoremap <silent>  dd :call vimspector#Launch()
nnoremap <silent>  rr :Semshi rename
nnoremap  p :echo expand('%')
nnoremap  sv :source $MYVIMRC
nnoremap  ev :vsplit $MYVIMRC
nnoremap  o :only
nnoremap <silent>  q :bd
nnoremap    
nnoremap <silent> # #zz
xnoremap <silent> * :call muchomuchacho#functions#VisualSelection('', '')/=@/
nnoremap <silent> * *zz
nmap - <Plug>(dirvish_up)
nnoremap / /\v
nnoremap ? ?\v
nmap <silent> E <Plug>(coc-diagnostic-prev)
map H ^
xnoremap <silent> J :call muchomuchacho#functions#SearchVisualSelectionWithAg()
map L $
nnoremap <silent> N Nzz
xmap S <Plug>VSurround
nnoremap <silent> T :call muchomuchacho#functions#GrepForWord("GGrep")
nmap <silent> W <Plug>(coc-diagnostic-next)
nmap [g <Plug>(coc-git-prevchunk)
nnoremap \e :edit =expand('%:p:h') . '/'
nnoremap <silent> \dl :diffget //3
nnoremap <silent> \dh :diffget //2
nnoremap <silent> \dd :Gvdiff
nnoremap <silent> \ds :Git
nnoremap \ w :call muchomuchacho#functions#GrepForWord("Rgw")
nmap ]g <Plug>(coc-git-nextchunk)
xmap ag <Plug>(coc-git-chunk-outer)
omap ag <Plug>(coc-git-chunk-outer)
nmap cr <Plug>(abolish-coerce-word)
nmap cS <Plug>CSurround
nmap cs <Plug>Csurround
nmap ds <Plug>Dsurround
nmap gs <Plug>(coc-git-chunkinfo)
nmap <silent> gr <Plug>(coc-references)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gd <Plug>(coc-definition)
vmap gx <Plug>NetrwBrowseXVis
nmap gx <Plug>NetrwBrowseX
nmap gcu <Plug>Commentary<Plug>Commentary
nmap gcc <Plug>CommentaryLine
omap gc <Plug>Commentary
nmap gc <Plug>(coc-git-commit)
xmap gc <Plug>Commentary
xmap gS <Plug>VgSurround
nnoremap <silent> g* g*zz
xmap ig <Plug>(coc-git-chunk-inner)
omap ig <Plug>(coc-git-chunk-inner)
nnoremap j gj
nnoremap k gk
nnoremap <silent> n nzz
nmap ySS <Plug>YSsurround
nmap ySs <Plug>YSsurround
nmap yss <Plug>Yssurround
nmap yS <Plug>YSurround
nmap ys <Plug>Ysurround
xnoremap <silent> <Plug>(coc-git-chunk-outer) :call coc#rpc#request('doKeymap', ['git-chunk-outer'])
onoremap <silent> <Plug>(coc-git-chunk-outer) :call coc#rpc#request('doKeymap', ['git-chunk-outer'])
xnoremap <silent> <Plug>(coc-git-chunk-inner) :call coc#rpc#request('doKeymap', ['git-chunk-inner'])
onoremap <silent> <Plug>(coc-git-chunk-inner) :call coc#rpc#request('doKeymap', ['git-chunk-inner'])
nnoremap <silent> <Plug>(coc-git-commit) :call coc#rpc#notify('doKeymap', ['git-commit'])
nnoremap <silent> <Plug>(coc-git-chunkinfo) :call coc#rpc#notify('doKeymap', ['git-chunkinfo'])
nnoremap <silent> <Plug>(coc-git-keepincoming) :call coc#rpc#notify('doKeymap', ['git-keepincoming'])
nnoremap <silent> <Plug>(coc-git-keepcurrent) :call coc#rpc#notify('doKeymap', ['git-keepcurrent'])
nnoremap <silent> <Plug>(coc-git-prevconflict) :call coc#rpc#notify('doKeymap', ['git-prevconflict'])
nnoremap <silent> <Plug>(coc-git-nextconflict) :call coc#rpc#notify('doKeymap', ['git-nextconflict'])
nnoremap <silent> <Plug>(coc-git-prevchunk) :call coc#rpc#notify('doKeymap', ['git-prevchunk'])
nnoremap <silent> <Plug>(coc-git-nextchunk) :call coc#rpc#notify('doKeymap', ['git-nextchunk'])
nmap <silent> <C-X>x <Plug>Commentary
xmap <silent> <C-X> <Plug>Commentary
vnoremap <silent> <Plug>NetrwBrowseXVis :call netrw#BrowseXVis()
nnoremap <silent> <Plug>NetrwBrowseX :call netrw#BrowseX(netrw#GX(),netrw#CheckIfRemote(netrw#GX()))
nmap <silent> <Plug>CommentaryUndo :echoerr "Change your <Plug>CommentaryUndo map to <Plug>Commentary<Plug>Commentary"
nnoremap <silent> <Plug>SurroundRepeat .
nnoremap <Plug>(Scalpel) :Scalpel/\v<=escape(expand('<cword>'), "!\"#$%&'()*+,-.\/:;<=>?@[\\]^`{|}~")>//<Left>
nnoremap <silent> <Plug>(dirvish_vsplit_up) :exe 'vsplit +Dirvish\ %:p'.repeat(':h',v:count1)
nnoremap <silent> <Plug>(dirvish_split_up) :exe 'split +Dirvish\ %:p'.repeat(':h',v:count1)
nnoremap <silent> <Plug>(dirvish_up) :exe 'Dirvish %:p'.repeat(':h',v:count1)
nmap <F12> <Plug>VimspectorStepOut
nmap <F11> <Plug>VimspectorStepInto
nmap <F10> <Plug>VimspectorStepOver
nmap <F9> <Plug>VimspectorToggleBreakpoint
nmap <F6> <Plug>VimspectorPause
nmap <F4> <Plug>VimspectorRestart
nmap <F3> <Plug>VimspectorStop
xnoremap <silent> <Plug>VimspectorBalloonEval :call vimspector#ShowEvalBalloon( 1 )
nnoremap <silent> <Plug>VimspectorBalloonEval :call vimspector#ShowEvalBalloon( 0 )
nnoremap <silent> <Plug>VimspectorRunToCursor :call vimspector#RunToCursor()
nnoremap <silent> <Plug>VimspectorStepOut :call vimspector#StepOut()
nnoremap <silent> <Plug>VimspectorStepInto :call vimspector#StepInto()
nnoremap <silent> <Plug>VimspectorStepOver :call vimspector#StepOver()
nnoremap <silent> <Plug>VimspectorAddFunctionBreakpoint :call vimspector#AddFunctionBreakpoint( expand( '<cexpr>' ) )
nnoremap <silent> <Plug>VimspectorToggleConditionalBreakpoint :call vimspector#ToggleBreakpoint( { 'condition': input( 'Enter condition expression: ' ),   'hitCondition': input( 'Enter hit count expression: ' ) } )
nnoremap <silent> <Plug>VimspectorToggleBreakpoint :call vimspector#ToggleBreakpoint()
nnoremap <silent> <Plug>VimspectorPause :call vimspector#Pause()
nnoremap <silent> <Plug>VimspectorRestart :call vimspector#Restart()
nnoremap <silent> <Plug>VimspectorStop :call vimspector#Stop()
nnoremap <silent> <Plug>VimspectorContinue :call vimspector#Continue()
nnoremap <silent> <Plug>(startify-open-buffers) :call startify#open_buffers()
onoremap <silent> <Plug>(fzf-maps-o) :call fzf#vim#maps('o', 0)
xnoremap <silent> <Plug>(fzf-maps-x) :call fzf#vim#maps('x', 0)
nnoremap <silent> <Plug>(fzf-maps-n) :call fzf#vim#maps('n', 0)
onoremap <silent> <Plug>(coc-classobj-a) :call coc#rpc#request('selectSymbolRange', [v:false, '', ['Interface', 'Struct', 'Class']])
onoremap <silent> <Plug>(coc-classobj-i) :call coc#rpc#request('selectSymbolRange', [v:true, '', ['Interface', 'Struct', 'Class']])
vnoremap <silent> <Plug>(coc-classobj-a) :call coc#rpc#request('selectSymbolRange', [v:false, visualmode(), ['Interface', 'Struct', 'Class']])
vnoremap <silent> <Plug>(coc-classobj-i) :call coc#rpc#request('selectSymbolRange', [v:true, visualmode(), ['Interface', 'Struct', 'Class']])
onoremap <silent> <Plug>(coc-funcobj-a) :call coc#rpc#request('selectSymbolRange', [v:false, '', ['Method', 'Function']])
onoremap <silent> <Plug>(coc-funcobj-i) :call coc#rpc#request('selectSymbolRange', [v:true, '', ['Method', 'Function']])
vnoremap <silent> <Plug>(coc-funcobj-a) :call coc#rpc#request('selectSymbolRange', [v:false, visualmode(), ['Method', 'Function']])
vnoremap <silent> <Plug>(coc-funcobj-i) :call coc#rpc#request('selectSymbolRange', [v:true, visualmode(), ['Method', 'Function']])
nnoremap <silent> <Plug>(coc-cursors-position) :call coc#rpc#request('cursorsSelect', [bufnr('%'), 'position', 'n'])
nnoremap <silent> <Plug>(coc-cursors-word) :call coc#rpc#request('cursorsSelect', [bufnr('%'), 'word', 'n'])
vnoremap <silent> <Plug>(coc-cursors-range) :call coc#rpc#request('cursorsSelect', [bufnr('%'), 'range', visualmode()])
nnoremap <silent> <Plug>(coc-refactor) :call       CocActionAsync('refactor')
nnoremap <silent> <Plug>(coc-command-repeat) :call       CocAction('repeatCommand')
nnoremap <silent> <Plug>(coc-float-jump) :call       coc#float#jump()
nnoremap <silent> <Plug>(coc-float-hide) :call       coc#float#close_all()
nnoremap <silent> <Plug>(coc-fix-current) :call       CocActionAsync('doQuickfix')
nnoremap <silent> <Plug>(coc-openlink) :call       CocActionAsync('openLink')
nnoremap <silent> <Plug>(coc-references-used) :call       CocActionAsync('jumpUsed')
nnoremap <silent> <Plug>(coc-references) :call       CocActionAsync('jumpReferences')
nnoremap <silent> <Plug>(coc-type-definition) :call       CocActionAsync('jumpTypeDefinition')
nnoremap <silent> <Plug>(coc-implementation) :call       CocActionAsync('jumpImplementation')
nnoremap <silent> <Plug>(coc-declaration) :call       CocActionAsync('jumpDeclaration')
nnoremap <silent> <Plug>(coc-definition) :call       CocActionAsync('jumpDefinition')
nnoremap <silent> <Plug>(coc-diagnostic-prev-error) :call       CocActionAsync('diagnosticPrevious', 'error')
nnoremap <silent> <Plug>(coc-diagnostic-next-error) :call       CocActionAsync('diagnosticNext',     'error')
nnoremap <silent> <Plug>(coc-diagnostic-prev) :call       CocActionAsync('diagnosticPrevious')
nnoremap <silent> <Plug>(coc-diagnostic-next) :call       CocActionAsync('diagnosticNext')
nnoremap <silent> <Plug>(coc-diagnostic-info) :call       CocActionAsync('diagnosticInfo')
nnoremap <silent> <Plug>(coc-format) :call       CocActionAsync('format')
nnoremap <silent> <Plug>(coc-rename) :call       CocActionAsync('rename')
nnoremap <Plug>(coc-codeaction-cursor) :call       CocActionAsync('codeAction',         'cursor')
nnoremap <Plug>(coc-codeaction-line) :call       CocActionAsync('codeAction',         'line')
nnoremap <Plug>(coc-codeaction) :call       CocActionAsync('codeAction',         '')
vnoremap <silent> <Plug>(coc-codeaction-selected) :call       CocActionAsync('codeAction',         visualmode())
vnoremap <silent> <Plug>(coc-format-selected) :call       CocActionAsync('formatSelected',     visualmode())
nnoremap <Plug>(coc-codelens-action) :call       CocActionAsync('codeLensAction')
nnoremap <Plug>(coc-range-select) :call       CocActionAsync('rangeSelect',     '', v:true)
vnoremap <silent> <Plug>(coc-range-select-backward) :call       CocActionAsync('rangeSelect',     visualmode(), v:false)
vnoremap <silent> <Plug>(coc-range-select) :call       CocActionAsync('rangeSelect',     visualmode(), v:true)
nmap <F5> <Plug>VimspectorContinue
xnoremap <C-L> l
xnoremap <C-K> k
xnoremap <C-J> j
xnoremap <C-H> h
tnoremap <C-L> l
tnoremap <C-K> k
tnoremap <C-J> j
tnoremap <C-H> h
nmap <F8> <Plug>VimspectorAddFunctionBreakpoint
nnoremap <silent> <C-W>b :Bck
nnoremap <C-W>r :GrepWiki 
nnoremap <C-W>p :Files ~/git/md_wiki
nnoremap <silent> <C-P> :Files
nnoremap <silent> <C-G> :Rgw
nnoremap <C-T>t "=strftime("%c")P
nnoremap <silent> <Right> :bn
nnoremap <silent> <Left> :bp
nnoremap <C-L> l
nnoremap <C-K> k
nnoremap <C-J> j
nnoremap <C-H> h
nnoremap <Down> <Nop>
nnoremap <Up> <Nop>
nnoremap <silent> <C-C> :let @/=""
vmap <BS> "-d
vmap <D-x> "*d
vmap <D-c> "*y
vmap <D-v> "-d"*P
nmap <D-v> "*P
imap S <Plug>ISurround
imap s <Plug>Isurround
inoremap  h
inoremap <NL> j
inoremap  k
inoremap  l
inoremap <expr>  complete_info()["selected"] != "-1" ? "\" : "\u\"
imap  <Plug>Isurround
inoremap <silent> <expr> " coc#_insert_key('request', 'iIg==0')
cnoremap %s/ %sm/
inoremap <silent> <expr> ' coc#_insert_key('request', 'iJw==0')
inoremap <silent> <expr> ( coc#_insert_key('request', 'iKA==0')
inoremap <silent> <expr> ) coc#_insert_key('request', 'iKQ==0')
inoremap <silent> <expr> < coc#_insert_key('request', 'iPA==0')
inoremap <silent> <expr> > coc#_insert_key('request', 'iPg==0')
inoremap <silent> <expr> [ coc#_insert_key('request', 'iWw==0')
inoremap <silent> <expr> ] coc#_insert_key('request', 'iXQ==0')
inoremap <silent> <expr> ` coc#_insert_key('request', 'iYA==0')
inoremap <silent> <expr> { coc#_insert_key('request', 'iew==0')
inoremap <silent> <expr> } coc#_insert_key('request', 'ifQ==0')
inoreabbr VARIENT VARIANT
inoreabbr Varient Variant
inoreabbr varient variant
inoreabbr VARIENTS VARIANTS
inoreabbr varients variants
inoreabbr Varients Variants
inoreabbr updaet update
inoreabbr UPDAET UPDATE
inoreabbr UPDAETED UPDATEED
inoreabbr updaetes updatees
inoreabbr Updaeted Updateed
inoreabbr Updaetes Updatees
inoreabbr UPDAETES UPDATEES
inoreabbr Updaet Update
inoreabbr updaeted updateed
inoreabbr teh the
inoreabbr HTE THE
inoreabbr hte the
inoreabbr Teh The
inoreabbr Hte The
inoreabbr TEH THE
inoreabbr submodlues submodules
inoreabbr SUBMODLUE SUBMODULE
inoreabbr submodlue submodule
inoreabbr Submodlues Submodules
inoreabbr Submodlue Submodule
inoreabbr SUBMODLUES SUBMODULES
inoreabbr Strinfigy Stringify
inoreabbr strinfigy stringify
inoreabbr STRINFIGY STRINGIFY
inoreabbr provied provide
inoreabbr PROVIEDD PROVIDED
inoreabbr Proviedd Provided
inoreabbr PROVIED PROVIDE
inoreabbr Provieds Provides
inoreabbr provieds provides
inoreabbr proviedd provided
inoreabbr Provied Provide
inoreabbr PROVIEDS PROVIDES
inoreabbr paramater parameter
inoreabbr Paramater Parameter
inoreabbr PARAMATER PARAMETER
inoreabbr FUNCITONS FUNCTIONS
inoreabbr FUNCITON FUNCTION
inoreabbr FUNCITONED FUNCTIONED
inoreabbr funcitoned functioned
inoreabbr funcitons functions
inoreabbr Funciton Function
inoreabbr funciton function
inoreabbr Funcitoned Functioned
inoreabbr Funcitons Functions
inoreabbr AHVE HAVE
inoreabbr Ahve Have
inoreabbr ahve have
inoreabbr Aboud About
inoreabbr ABOUD ABOUT
inoreabbr aboud about
let &cpo=s:cpo_save
unlet s:cpo_save
set autoindent
set background=dark
set backspace=2
set backupskip=/private/tmp/*,*.re,*.rei
set belloff=all
set clipboard=unnamed
set cmdheight=2
set expandtab
set fileencodings=ucs-bom,utf-8,default,latin1
set formatoptions=tcqjn
set helplang=en
set hidden
set highlight=8:SpecialKey,~:EndOfBuffer,@:NonText,d:Directory,e:ErrorMsg,i:IncSearch,l:Search,m:MoreMsg,M:ModeMsg,n:LineNr,a:LineNrAbove,b:LineNrBelow,N:CursorLineNr,r:Question,s:StatusLine,S:StatusLineNC,c:VertSplit,t:Title,v:Visual,V:VisualNOS,w:WarningMsg,W:WildMenu,f:Folded,F:FoldColumn,A:DiffAdd,C:DiffChange,D:DiffDelete,T:DiffText,>:SignColumn,-:Conceal,B:SpellBad,P:SpellCap,R:SpellRare,L:SpellLocal,+:Pmenu,=:PmenuSel,x:PmenuSbar,X:PmenuThumb,*:TabLine,#:TabLineSel,_:TabLineFill,!:CursorColumn,.:CursorLine,o:ColorColumn,q:QuickFixLine,z:StatusLineTerm,Z:StatusLineTermNC,@:Conceal,D:Conceal,N:FoldColumn,c:LineNr
set nojoinspaces
set laststatus=2
set lazyredraw
set listchars=nbsp:⦸,tab:▷┅,extends:»,precedes:«,trail:���
set nomodeline
set runtimepath=
set runtimepath+=~/.vim
set runtimepath+=~/.vim/pack/bundle/opt/vim-abolish
set runtimepath+=~/.vim/pack/bundle/opt/vim-devicons
set runtimepath+=~/.vim/pack/bundle/opt/vim-airline-themes
set runtimepath+=~/.vim/pack/bundle/opt/vim-airline
set runtimepath+=~/.vim/pack/bundle/opt/gruvbox
set runtimepath+=~/.vim/pack/bundle/opt/semshi
set runtimepath+=~/.vim/pack/bundle/opt/coc.nvim
set runtimepath+=~/.vim/pack/bundle/opt/vim-markdown
set runtimepath+=~/.vim/pack/bundle/opt/vim-yaml
set runtimepath+=~/.vim/pack/bundle/opt/vim-toml
set runtimepath+=~/.vim/pack/bundle/opt/vim-rooter
set runtimepath+=~/.vim/pack/bundle/opt/fzf.vim
set runtimepath+=~/.vim/pack/bundle/opt/vim-startify
set runtimepath+=~/.vim/pack/bundle/opt/vimspector
set runtimepath+=~/.vim/pack/bundle/opt/vim-flutter
set runtimepath+=~/.vim/pack/bundle/opt/dart-vim-plugin
set runtimepath+=~/.vim/pack/bundle/opt/tagbar
set runtimepath+=~/.vim/pack/bundle/opt/vimwiki
set runtimepath+=~/.vim/pack/bundle/opt/vim-dirvish
set runtimepath+=~/.vim/pack/bundle/opt/vim-todo
set runtimepath+=~/.vim/pack/bundle/opt/scalpel
set runtimepath+=~/.vim/pack/bundle/opt/neomake
set runtimepath+=~/.vim/pack/bundle/opt/vim-repeat
set runtimepath+=~/.vim/pack/bundle/opt/vim-mundo
set runtimepath+=~/.vim/pack/bundle/opt/vim-surround
set runtimepath+=~/.vim/pack/bundle/opt/vim-fugitive
set runtimepath+=~/.vim/pack/bundle/opt/vim-commentary
set runtimepath+=/usr/share/vim/vimfiles
set runtimepath+=/usr/share/vim/vim81
set runtimepath+=~/.vim/pack/bundle/opt/vim-markdown/after
set runtimepath+=~/.vim/pack/bundle/opt/vim-yaml/after
set runtimepath+=/usr/share/vim/vimfiles/after
set runtimepath+=~/.vim/after
set scrolloff=3
set shell=sh
set shiftround
set shiftwidth=2
set shortmess=filnxSAOTWacotI
set showbreak=↳\ 
set showmatch
set showtabline=2
set sidescrolloff=3
set smarttab
set softtabstop=-1
set spellcapcheck=
set splitbelow
set splitright
set statusline=%{coc#status()}%{get(b:,'coc_current_function','')}
set noswapfile
set swapsync=
set switchbuf=usetab
set synmaxcol=200
set tabline=%!airline#extensions#tabline#get()
set tabstop=2
set textwidth=100
set timeoutlen=300
set updatecount=80
set updatetime=300
set viewdir=~/.vim/tmp/view
set viewoptions=cursor,folds
set viminfo=
set visualbell
set whichwrap=b,h,l,s,<,>,[,],~
set wildcharm=26
set wildignore=*.o,*.rej
set wildmenu
set wildmode=longest,list
set window=0
set nowritebackup
let s:so_save = &so | let s:siso_save = &siso | set so=0 siso=0
let v:this_session=expand("<sfile>:p")
silent only
silent tabonly
cd ~/git/UniRAT
if expand('%') == '' && !&modified && line('$') <= 1 && getline(1) == ''
  let s:wipebuf = bufnr('%')
endif
set shortmess=aoO
argglobal
%argdel
$argadd /private/etc/docker/daemon.json
edit /private/etc/docker/daemon.json
set splitbelow splitright
wincmd t
set winminheight=0
set winheight=1
set winminwidth=0
set winwidth=1
argglobal
noremap <buffer> j gj
noremap <buffer> k gk
setlocal autoindent
setlocal backupcopy=
setlocal nobinary
set breakindent
setlocal breakindent
set breakindentopt=shift:2
setlocal breakindentopt=shift:2
setlocal bufhidden=
setlocal buflisted
setlocal buftype=
setlocal nocindent
setlocal cinkeys=0{,0},0),0],:,0#,!^F,o,O,e
setlocal cinoptions=
setlocal cinwords=if,else,while,do,for,switch
setlocal colorcolumn=
setlocal comments=
setlocal commentstring=
setlocal complete=.,w,b,u,t,i
setlocal completefunc=
setlocal nocopyindent
setlocal cryptmethod=
setlocal nocursorbind
setlocal nocursorcolumn
set cursorline
setlocal cursorline
setlocal cursorlineopt=both
setlocal define=
setlocal dictionary=
setlocal nodiff
setlocal equalprg=
setlocal errorformat=
setlocal expandtab
if &filetype != 'json'
setlocal filetype=json
endif
setlocal fixendofline
setlocal foldcolumn=0
setlocal foldenable
setlocal foldexpr=0
setlocal foldignore=#
setlocal foldlevel=0
setlocal foldmarker={{{,}}}
setlocal foldmethod=manual
setlocal foldminlines=1
setlocal foldnestmax=20
setlocal foldtext=foldtext()
setlocal formatexpr=
setlocal formatoptions=cqjn
setlocal formatlistpat=^\\s*\\d\\+[\\]:.)}\\t\ ]\\s*
setlocal formatprg=
setlocal grepprg=
setlocal iminsert=0
setlocal imsearch=-1
setlocal include=
setlocal includeexpr=
setlocal indentexpr=GetJSONIndent()
setlocal indentkeys=0{,0},0),0[,0],!^F,o,O,e
setlocal noinfercase
setlocal iskeyword=@,48-57,_,192-255
setlocal keywordprg=
set linebreak
setlocal linebreak
setlocal nolisp
setlocal lispwords=
set list
setlocal list
setlocal makeencoding=
setlocal makeprg=
setlocal matchpairs=(:),{:},[:]
setlocal nomodeline
setlocal modifiable
setlocal nrformats=bin,octal,hex
set number
setlocal number
setlocal numberwidth=4
setlocal omnifunc=
setlocal path=
setlocal nopreserveindent
setlocal nopreviewwindow
setlocal quoteescape=\\
setlocal noreadonly
set relativenumber
setlocal relativenumber
setlocal noscrollbind
setlocal scrolloff=-1
setlocal shiftwidth=2
setlocal noshortname
setlocal showbreak=
setlocal sidescrolloff=-1
set signcolumn=yes
setlocal signcolumn=yes
setlocal nosmartindent
setlocal softtabstop=-1
setlocal spell
setlocal spellcapcheck=
setlocal spellfile=~/.vim/spell/en.utf-8.add
setlocal spelllang=en,es
setlocal statusline=%!airline#statusline(1)
setlocal suffixesadd=
setlocal noswapfile
setlocal synmaxcol=200
if &syntax != 'json'
setlocal syntax=json
endif
setlocal tabstop=2
setlocal tagcase=
setlocal tagfunc=
setlocal tags=
setlocal termwinkey=
setlocal termwinscroll=10000
setlocal termwinsize=
setlocal textwidth=100
setlocal thesaurus=
setlocal noundofile
setlocal undolevels=-123456
setlocal wincolor=
setlocal nowinfixheight
setlocal nowinfixwidth
setlocal wrap
setlocal wrapmargin=0
silent! normal! zE
let s:l = 1 - ((0 * winheight(0) + 36) / 73)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
1
normal! 0
tabnext 1
badd +0 /private/etc/docker/daemon.json
if exists('s:wipebuf') && len(win_findbuf(s:wipebuf)) == 0
  silent exe 'bwipe ' . s:wipebuf
endif
unlet! s:wipebuf
set winheight=1 winwidth=20 shortmess=filnxSAOTWacotI
set winminheight=1 winminwidth=1
let s:sx = expand("<sfile>:p:r")."x.vim"
if file_readable(s:sx)
  exe "source " . fnameescape(s:sx)
endif
let &so = s:so_save | let &siso = s:siso_save
doautoall SessionLoadPost
unlet SessionLoad
" vim: set ft=vim :
