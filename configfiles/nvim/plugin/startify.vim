let g:startify_session_dir = '~/.vim/tmp/session'

let g:startify_lists = [
      \ { 'type': 'files',     'header': ['   Files']          },
      \ { 'type': 'dir',       'header': ['   Current Directory: '.getcwd()] },
      \ { 'type': 'sessions',  'header': ['   Sessions']       },
      \ { 'type': 'bookmarks', 'header': ['   Bookmarks']      },
      \ ]

let g:startify_bookmarks = [
            \ { 'd': '~/.dotfiles' },
            \ { 'e': '~/.exports' },
            \ { 'z': '~/.zshrc' },
            \ { 'i3': '~/.config/i3/config' },
            \ '~/git',
            \ '~/code',
            \ ]
