" Insert mode mappings

" Add date to current line
inoremap <silent> <special> <F2> <C-r>=strftime("%d-%m-%Y")<CR>

" use ALT+{h, j, k l} to navigate widows
inoremap <C-h> <C-\><C-N><C-w>h
inoremap <C-j> <C-\><C-N><C-w>j
inoremap <C-k> <C-\><C-N><C-w>k
inoremap <C-l> <C-\><C-N><C-w>l

" No arrow keys --- force yourself to use the home row
inoremap <up> <nop>
inoremap <down> <nop>
inoremap <left> <nop>
inoremap <right> <nop>

