" map <ESC> to exit terminal mode
tnoremap <Esc> <C-\><C-n>

" use ALT+{h, j, k l} to navigate widows
tnoremap <C-h> <C-\><C-N><C-w>h
tnoremap <C-j> <C-\><C-N><C-w>j
tnoremap <C-k> <C-\><C-N><C-w>k
tnoremap <C-l> <C-\><C-N><C-w>l
