augroup md_wiki
  autocmd!
  " populate diary page from template
  autocmd BufNewFile *md_wiki/diary/*.md :silent 0r !generate-vimwiki-diary-template '%'
  " update diary todo section and ToDo.md with any changes
  autocmd BufRead,BufNewFile *md_wiki/diary/*.md call AppendToDo()
  autocmd BufWritePre,FileWritePre *md_wiki/diary/*.md call UpdateToDoFile()
  autocmd BufRead *md_wiki/home.md :Git pull --rebase
  autocmd BufRead *md_wiki/home.md call AddLinkToReading()
  autocmd BufWritePost *md_wiki/* call RunGitCommands()
augroup END

" Add modified files, create a comma separated string with them and use them
" as a commit message.
function! RunGitCommands() abort
  let choice = confirm("Commit and push changes?", "&Yes\n&No\n&Cancel", 3)
  if choice == 2
    echo "Exiting without committing changes!"
    return
  elseif choice == 1
    exe 'Git add .'
    let staged_files = join(split(execute("Git status -s"), '[MA]  '), '')
    let files_str = join(split(staged_files, '\n'), ',')
    if empty(files_str) == 1
      echo "No modified files to commit."
      return
    endif
    exe "Git commit -m 'Update '" . files_str
    exe 'Git push'
    echom 'Changes commited and pushed for ' . files_str
  endif
endfunction

" Add link to Reading after line
function! AddLinkToReading() abort
  let mark_line =  search('\[Schedule\]')
  if mark_line
    call deletebufline(bufnr("%"), mark_line)
  endif
  let w = search('### Active')
  exec w . "pu='* [Schedule](/diary/'.strftime('%Y-%m-%d').'#Schedule)'"
  silent update
endfunction

function! AppendToDo() abort
  if expand("%:t:r") == strftime('%Y-%m-%d')
    let todo_list = readfile("ToDo.md")
    let idx = index(todo_list, '## Future Tasks')
    let todo_content = todo_list[:idx - 3]
    call MoveText(todo_content)
  endif
endfunction

function! UpdateToDoFile() abort
  let diary_list = getbufline(bufnr("%"), 1, "$")
  let start_idx = index(diary_list, '## Current Tasks')
  if start_idx == -1
    return
  endif
  let end_idx = index(diary_list, '## Input')
  let diary_content = diary_list[start_idx:end_idx - 3]
  call extend(diary_content, ["", ""])
  let todo_list = readfile("ToDo.md")
  let td_start_idx = index(todo_list, '## Future Tasks')
  let td_end_idx = index(todo_list, '$')
  let todo_content = todo_list[td_start_idx:td_end_idx]
  call extend(diary_content, todo_content)
  call writefile(diary_content, "ToDo.md", "b")
endfunction

function! MoveText(text_content) abort
  let mark_start = search('## Current Tasks')
  let mark_end = search('## Input')
  if mark_start
    " delete current ToDo, that means keeping blank lines above mark_end
    call deletebufline(bufnr("%"), mark_start, mark_end - 3)
    " append ToDo content right form mark_1 position
    call append(mark_start - 1, a:text_content)
  else
    " in the absence of a current ToDo section use mark_2 as reference
    " Add 2 blank lines to comply with end-of-section double spacing
    call extend(a:text_content, ["", ""])
    call append(mark_end - 1, a:text_content)
  endif
endfunction
