local api = vim.api
local M = {}

function M.makeScratch()
  local stats = api.nvim_list_uis()[1]
  local width = stats.width
  local height = stats.height

  local window_opts = {
      relative = "editor",
      width = width - 15,
      height = height - 20,
      col = 10,
      row = 10,
      focusable = false,
    }

  local bufh = api.nvim_create_buf(false, true)
  local win = api.nvim_open_win(bufh, true, window_opts)

  return win
end

return M
